from time import time, sleep

from operations import *
from utils import *


display_balances(None)
_, last_round_time = get_last_block_timestamp()
print("last_round_time = ", last_round_time)

# create an asset
nft_amount = 1

nft_id = send_txn(
    seller.getPrivateKey(),
    transaction.AssetCreateTxn(
        sender=seller.getAddress(),
        total=nft_amount,
        decimals=0,
        default_frozen=False,
        manager=seller.getAddress(),
        reserve=seller.getAddress(),
        freeze=seller.getAddress(),
        clawback=seller.getAddress(),
        unit_name="tweets",
        asset_name="My tweet",
        url=f"https://dummy.asset/1",
        note=b"this is my tweet to sell",
        sp=algod_client.suggested_params()
    )
).assetIndex
print("nft_id =", nft_id)

# create an auction lasting 30 seconds
start_time = int(time()) + 10  # now + 10 sec
end_time = start_time + 30  # start + 30 sec
reserve = 1_000_000  # 1 Algo
increment = 100_000  # 0.1 Algo
print("Creator is creating an auction that lasts 30 seconds to auction off the NFT...")
print("start_time", start_time)
print("end_time", end_time)

app_id = create_auction_app(
    sender=creator,
    seller_address=seller.getAddress(),
    nft_id=nft_id,
    start_time=start_time,
    end_time=end_time,
    reserve=reserve,
    min_bid_increment=increment
)
app_address: str = get_application_address(app_id)
print(
    "Done. The auction app ID is",
    app_id,
    "and the escrow account is",
    app_address,
    "\n",
)

print("Seller is setting up and funding NFT auction...")
setup_auction_app(
    app_id=app_id,
    funder=creator,
    ntf_holder=seller,
    nft_id=nft_id,
    nft_amount=nft_amount
)
print("Done\n")

# wait couple of secs
display_balances(app_address)

_, last_round_time = get_last_block_timestamp()
# sleeping_period = (start_time - last_round_time - 85000 + 5)
sleeping_period = 5
print("Let's wait", sleeping_period, "seconds")
if sleeping_period > 0:
    sleep(sleeping_period)
print("Woke up")
display_balances(app_address)

# place a bid
bid_amount = reserve
print("Bidder is placing bid for", bid_amount)

place_bid(
    app_id=app_id,
    bidder=bidder,
    bid_amount=bid_amount
)
print("Done\n")

# opt in to asset
print("Bidder is opting in to the asset", nft_id)
opt_in_to_asset(nft_id, bidder)
print("Done\n")

_, last_round_time = get_last_block_timestamp()
# sleeping_period = end_time + 5 - last_round_time
sleeping_period=10
if sleeping_period > 0:
    print("Let's wait", sleeping_period, "seconds")
    sleep(sleeping_period)
print("Woke up")

print("Seller is closing the auction")
close_auction(app_id, seller)
print("Let's wait", sleeping_period, "seconds")
sleep(sleeping_period)

display_balances(app_address)

# TODO there is a bug
# It's still does not work 100%, I modified the contract code
# so it doesn't wait few hours, but somehow when teh auction is done
# we do not transfer the NFT to the winner :D I need to debug the contract code for close condition
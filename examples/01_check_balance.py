from algosdk.v2client.algod import AlgodClient

from config import *

algod_client = AlgodClient(ALGOD_TOKEN, ALGOD_ADDRESS)
account_info = algod_client.account_info(ACCOUNT_ADDRESS1)
print("addr1 balance: {} microAlgos".format(account_info.get('amount')) + "\n")

from algosdk import account, encoding, mnemonic
from algosdk.future import transaction
from algosdk.logic import get_application_address

from contracts import approval_program, clear_state_program
from account import Account
from utils import *


APPROVAL_PROGRAM = b""
CLEAR_STATE_PROGRAM = b""


def get_contracts() -> Tuple[bytes, bytes]:
    """Get the compiled TEAL contracts for the auction.

    Args:
        client: An algod client that has the ability to compile TEAL programs.

    Returns:
        A tuple of 2 byte strings. The first is the approval program, and the
        second is the clear state program.
    """
    global APPROVAL_PROGRAM
    global CLEAR_STATE_PROGRAM

    if len(APPROVAL_PROGRAM) == 0:
        APPROVAL_PROGRAM = fully_compile_contract(algod_client, approval_program())
        CLEAR_STATE_PROGRAM = fully_compile_contract(algod_client, clear_state_program())

    return APPROVAL_PROGRAM, CLEAR_STATE_PROGRAM


def create_auction_app(
        sender: Account,
        seller_address: str,
        nft_id: int,
        start_time: int,
        end_time: int,
        reserve: int,
        min_bid_increment: int,
) -> int:
    """Create a new auction.

    Args:
        client: An algod client.
        sender: The account that will create the auction application.
        seller_address: The address of the seller that currently holds the NFT being
            auctioned.
        nft_id: The ID of the NFT being auctioned.
        start_time: A UNIX timestamp representing the start time of the auction.
            This must be greater than the current UNIX timestamp.
        end_time: A UNIX timestamp representing the end time of the auction. This
            must be greater than startTime.
        reserve: The reserve amount of the auction. If the auction ends without
            a bid that is equal to or greater than this amount, the auction will
            fail, meaning the bid amount will be refunded to the lead bidder and
            the NFT will return to the seller.
        min_bid_increment: The minimum different required between a new bid and
            the current leading bid.

    Returns:
        The ID of the newly created auction app.
    """
    approval, clear = get_contracts()

    globalSchema = transaction.StateSchema(num_uints=7, num_byte_slices=2)
    localSchema = transaction.StateSchema(num_uints=0, num_byte_slices=0)

    app_args = [
        encoding.decode_address(seller_address),
        nft_id.to_bytes(8, "big"),
        start_time.to_bytes(8, "big"),
        end_time.to_bytes(8, "big"),
        reserve.to_bytes(8, "big"),
        min_bid_increment.to_bytes(8, "big"),
    ]

    txn = transaction.ApplicationCreateTxn(
        sender=sender.getAddress(),
        on_complete=transaction.OnComplete.NoOpOC,
        approval_program=approval,
        clear_program=clear,
        global_schema=globalSchema,
        local_schema=localSchema,
        app_args=app_args,
        sp=algod_client.suggested_params(),
    )

    signedTxn = txn.sign(sender.getPrivateKey())

    algod_client.send_transaction(signedTxn)

    response = wait_for_txn(signedTxn.get_txid())
    assert response.applicationIndex is not None and response.applicationIndex > 0
    return response.applicationIndex


def setup_auction_app(
        app_id: int,
        funder: Account,
        ntf_holder: Account,
        nft_id: int,
        nft_amount: int,
) -> None:
    """Finish setting up an auction.

    This operation funds the app auction escrow account, opts that account into
    the NFT, and sends the NFT to the escrow account, all in one atomic
    transaction group. The auction must not have started yet.

    The escrow account requires a total of 0.203 Algos for funding. See the code
    below for a breakdown of this amount.

    Args:
        client: An algod client.
        app_id: The app ID of the auction.
        funder: The account providing the funding for the escrow account.
        ntf_holder: The account holding the NFT.
        nft_id: The NFT ID.
        nft_amount: The NFT amount being auctioned. Some NFTs has a total supply
            of 1, while others are fractional NFTs with a greater total supply,
            so use a value that makes sense for the NFT being auctioned.
    """
    appAddr = get_application_address(app_id)

    suggestedParams = algod_client.suggested_params()

    fundingAmount = (
        # min account balance
            100_000
            # additional min balance to opt into NFT
            + 100_000
            # 3 * min txn fee
            + 3 * 1_000
    )

    fundAppTxn = transaction.PaymentTxn(
        sender=funder.getAddress(),
        receiver=appAddr,
        amt=fundingAmount,
        sp=suggestedParams,
    )

    setupTxn = transaction.ApplicationCallTxn(
        sender=funder.getAddress(),
        index=app_id,
        on_complete=transaction.OnComplete.NoOpOC,
        app_args=[b"setup"],
        foreign_assets=[nft_id],
        sp=suggestedParams,
    )

    fundNftTxn = transaction.AssetTransferTxn(
        sender=ntf_holder.getAddress(),
        receiver=appAddr,
        index=nft_id,
        amt=nft_amount,
        sp=suggestedParams,
    )

    transaction.assign_group_id([fundAppTxn, setupTxn, fundNftTxn])

    signedFundAppTxn = fundAppTxn.sign(funder.getPrivateKey())
    signedSetupTxn = setupTxn.sign(funder.getPrivateKey())
    signedFundNftTxn = fundNftTxn.sign(ntf_holder.getPrivateKey())

    algod_client.send_transactions([signedFundAppTxn, signedSetupTxn, signedFundNftTxn])

    wait_for_txn(signedFundAppTxn.get_txid())


def place_bid(app_id: int, bidder: Account, bid_amount: int) -> None:
    """Place a bid on an active auction.

    Args:
        client: An Algod client.
        app_id: The app ID of the auction.
        bidder: The account providing the bid.
        bid_amount: The amount of the bid.
    """
    appAddr = get_application_address(app_id)
    print("debug appAddr", appAddr)
    appGlobalState = get_app_global_state(app_id)
    print("debug appGlobalState", appGlobalState)

    nftID = appGlobalState[b"nft_id"]
    print("debug nftID", appGlobalState)

    if any(appGlobalState[b"bid_account"]):
        # if "bid_account" is not the zero address
        prevBidLeader = encoding.encode_address(appGlobalState[b"bid_account"])
    else:
        prevBidLeader = None
    print("debug prevBidLeader", prevBidLeader)

    suggestedParams = algod_client.suggested_params()

    payTxn = transaction.PaymentTxn(
        sender=bidder.getAddress(),
        receiver=appAddr,
        amt=bid_amount,
        sp=suggestedParams,
    )

    appCallTxn = transaction.ApplicationCallTxn(
        sender=bidder.getAddress(),
        index=app_id,
        on_complete=transaction.OnComplete.NoOpOC,
        app_args=[b"bid"],
        foreign_assets=[nftID],
        # must include the previous lead bidder here to the app can refund that bidder's payment
        accounts=[prevBidLeader] if prevBidLeader is not None else [],
        sp=suggestedParams,
    )

    transaction.assign_group_id([payTxn, appCallTxn])

    signedPayTxn = payTxn.sign(bidder.getPrivateKey())
    signedAppCallTxn = appCallTxn.sign(bidder.getPrivateKey())

    algod_client.send_transactions([signedPayTxn, signedAppCallTxn])

    wait_for_txn(appCallTxn.get_txid())


def close_auction(app_id: int, closer: Account):
    """Close an auction.

    This action can only happen before an auction has begun, in which case it is
    cancelled, or after an auction has ended.

    If called after the auction has ended and the auction was successful, the
    NFT is transferred to the winning bidder and the auction proceeds are
    transferred to the seller. If the auction was not successful, the NFT and
    all funds are transferred to the seller.

    Args:
        client: An Algod client.
        app_id: The app ID of the auction.
        closer: The account initiating the close transaction. This must be
            either the seller or auction creator if you wish to close the
            auction before it starts. Otherwise, this can be any account.
    """
    appGlobalState = get_app_global_state(app_id)

    nftID = appGlobalState[b"nft_id"]

    accounts: List[str] = [encoding.encode_address(appGlobalState[b"seller"])]

    if any(appGlobalState[b"bid_account"]):
        # if "bid_account" is not the zero address
        accounts.append(encoding.encode_address(appGlobalState[b"bid_account"]))

    deleteTxn = transaction.ApplicationDeleteTxn(
        sender=closer.getAddress(),
        index=app_id,
        accounts=accounts,
        foreign_assets=[nftID],
        sp=algod_client.suggested_params(),
    )
    signedDeleteTxn = deleteTxn.sign(closer.getPrivateKey())

    algod_client.send_transaction(signedDeleteTxn)

    wait_for_txn(signedDeleteTxn.get_txid())


def opt_in_to_asset(asset_id: int, account: Account) -> PendingTxnResponse:
    txn = transaction.AssetOptInTxn(
        sender=account.getAddress(),
        index=asset_id,
        sp=algod_client.suggested_params(),
    )
    signedTxn = txn.sign(account.getPrivateKey())

    algod_client.send_transaction(signedTxn)
    return wait_for_txn(signedTxn.get_txid())

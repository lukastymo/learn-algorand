from algosdk import kmd
from algosdk.future import transaction
from algosdk.v2client.algod import AlgodClient
from algosdk.wallet import Wallet

from config import *

algod_client = AlgodClient(ALGOD_TOKEN, ALGOD_ADDRESS)
kmd_client = kmd.KMDClient(KMD_TOKEN, KMD_ADDRESS)

# build unsigned tx
suggested_params = algod_client.suggested_params()
note = "Thanks for pizza".encode()
amount = 100000
txn = transaction.PaymentTxn(
    sender=ACCOUNT_ADDRESS1,
    receiver=ACCOUNT_ADDRESS2,
    amt=amount,
    sp=suggested_params,
    note=note
)

wallet = Wallet(WALLET_NAME, WALLET_PASSWORD, kmd_client)
private_key = wallet.export_key(ACCOUNT_ADDRESS1)
signed_txn = txn.sign(private_key)

txid = algod_client.send_transaction(signed_txn)
print("Successfully sent transaction with txID: {}".format(txid))

from operations import *

import pprint

pp = pprint.PrettyPrinter(indent=4)
sp = algod_client.suggested_params()


def to_algos(micro_algos: int) -> int:
    return (micro_algos * 1.0) / 10**6


def get_amount(acc: Account) -> int:
    return to_algos(algod_client.account_info(acc.addr)['amount'])

def display_info(acc: Account) -> None:
    print(f"picked account {acc.addr}")
    print(f"account info: {acc.addrShortHand}")
    print(f"amount: {to_algos(algod_client.account_info(acc.addr)['amount'])} Algo(s) \n")


funding_account = Account(wallet.export_key(ACCOUNT_ADDRESS1))
admin_account = Account(wallet.export_key(ACCOUNT_ADDRESS2))

display_info(funding_account)
display_info(admin_account)

txn_amount = 1_000_000
txn = transaction.PaymentTxn(
    sender=funding_account.addr,
    receiver=admin_account.addr,
    amt=txn_amount,
    sp=sp
)
signed_txn = txn.sign(funding_account.getPrivateKey())
print(f"Sending {txn_amount} Algo(s) from {funding_account.addrShortHand} to {admin_account.addrShortHand}")
print(f"Source (before) amount={get_amount(funding_account)}")
print(f"Target (before) amount={get_amount(admin_account)}")

txn_id = algod_client.send_transaction(signed_txn)
print(f"Sent, waiting for confirmation for tx id={txn_id}")

wait_for_txn(txn_id)
print(f"Confirmed, source new amount={get_amount(funding_account)}")
print(f"Confirmed, target new amount={get_amount(admin_account)}")

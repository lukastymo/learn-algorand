from base64 import b64decode
from typing import List, Dict, Any, Union, Optional, Tuple

from algosdk import kmd
from algosdk.v2client.algod import AlgodClient
from algosdk.wallet import Wallet
from pyteal import compileTeal, Mode, Expr

from config import *
from operations import Account


class PendingTxnResponse:
    def __init__(self, response: Dict[str, Any]) -> None:
        self.poolError: str = response["pool-error"]
        self.txn: Dict[str, Any] = response["txn"]

        self.applicationIndex: Optional[int] = response.get("application-index")
        self.assetIndex: Optional[int] = response.get("asset-index")
        self.closeRewards: Optional[int] = response.get("close-rewards")
        self.closingAmount: Optional[int] = response.get("closing-amount")
        self.confirmedRound: Optional[int] = response.get("confirmed-round")
        self.globalStateDelta: Optional[Any] = response.get("global-state-delta")
        self.localStateDelta: Optional[Any] = response.get("local-state-delta")
        self.receiverRewards: Optional[int] = response.get("receiver-rewards")
        self.senderRewards: Optional[int] = response.get("sender-rewards")

        self.innerTxns: List[Any] = response.get("inner-txns", [])
        self.logs: List[bytes] = [b64decode(l) for l in response.get("logs", [])]


algod_client: AlgodClient = AlgodClient(ALGOD_TOKEN, ALGOD_ADDRESS)
kmd_client = kmd.KMDClient(KMD_TOKEN, KMD_ADDRESS)
wallet = Wallet(WALLET_NAME, WALLET_PASSWORD, kmd_client)


def display_balances(app_addr):
    creator_balance = get_balance(algod_client, creator.getAddress())
    seller_balance = get_balance(algod_client, seller.getAddress())
    bidder_balance = get_balance(algod_client, bidder.getAddress())
    app_balance = get_balance(algod_client, app_addr) if app_addr else 0

    print("====================================")
    print("Creator's balance", creator_balance)
    print("Seller's balance", seller_balance)
    print("Bidder's balance", bidder_balance)
    print("Auction's balance", app_balance)
    print("====================================")
    print("\n")


def send_txn(priv_key, txn) -> PendingTxnResponse:
    signed_txn = txn.sign(priv_key)
    sent_txn_id = algod_client.send_transaction(signed_txn)
    print("Successfully sent transaction with txID: {}".format(sent_txn_id))
    response = wait_for_txn(signed_txn.get_txid())
    return response


creator = Account(wallet.export_key(ACCOUNT_ADDRESS1))  # Bob
seller = Account(wallet.export_key(ACCOUNT_ADDRESS2))  # Alice
bidder = Account(wallet.export_key(ACCOUNT_ADDRESS3))  # Carla


def fully_compile_contract(client: AlgodClient, contract: Expr) -> bytes:
    teal = compileTeal(
        ast=contract,
        mode=Mode.Application,
        version=5
    )
    response = client.compile(teal)
    return b64decode(response["result"])


def get_balance(client: AlgodClient, address: str) -> Dict[int, int]:
    account_info = client.account_info(address)
    balances: Dict[int, int] = dict()
    balances[0] = account_info.get('amount')
    assets: List[Dict[str, Any]] = account_info.get("assets", [])
    for assetHolding in assets:
        asset_id = assetHolding["asset-id"]
        amount = assetHolding["amount"]
        balances[asset_id] = amount
    return balances


def decode_state(state_array: List[Any]) -> Dict[bytes, Union[int, bytes]]:
    state: Dict[bytes, Union[int, bytes]] = dict()

    for pair in state_array:
        key = b64decode(pair["key"])

        value = pair["value"]
        value_type = value["type"]

        if value_type == 2:
            # value is uint64
            value = value.get("uint", 0)
        elif value_type == 1:
            # value is byte array
            value = b64decode(value.get("bytes", ""))
        else:
            raise Exception(f"Unexpected state type: {value_type}")

        state[key] = value

    return state


def wait_for_txn(txn_id: str, timeout: int = 10) -> PendingTxnResponse:
    last_status = algod_client.status()
    last_round = last_status["last-round"]
    start_round = last_round

    while last_round < start_round + timeout:
        pending_txn = algod_client.pending_transaction_info(txn_id)

        if pending_txn.get("confirmed-round", 0) > 0:
            return PendingTxnResponse(pending_txn)

        if pending_txn["pool-error"]:
            raise Exception("Pool error: {}".format(pending_txn["pool-error"]))

        last_round += 1

    raise Exception(
        "Transaction {} not confirmed after {} rounds".format(txn_id, timeout)
    )


def get_last_block_timestamp() -> Tuple[int, int]:
    status = algod_client.status()
    last_round = status["last-round"]
    block = algod_client.block_info(last_round)
    timestamp = block["block"]["ts"]

    return block, timestamp


def get_app_global_state(app_id: int) -> Dict[bytes, Union[int, bytes]]:
    app_info = algod_client.application_info(app_id)
    return decode_state(app_info["params"]["global-state"])
